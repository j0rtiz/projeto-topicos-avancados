{
    "openapi": "3.0.1",
    "info": {
        "title": "Projeto Tópicos Avançados",
        "version": "1.0.0",
        "description": "Product API"
    },
    "servers": [
        {
            "url": "http://localhost:4000/api",
            "description": "Local"
        }
    ],
    "paths": {
        "/products": {
            "post": {
                "tags": ["Product"],
                "summary": "Product registration",
                "operationId": "productCreate",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "name": {
                                        "type": "string",
                                        "example": "Batata"
                                    },
                                    "price": {
                                        "type": "number",
                                        "example": 10
                                    }
                                },
                                "required": ["name", "price"]
                            }
                        }
                    },
                    "required": true
                },
                "responses": {
                    "201": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "name": {
                                            "type": "string",
                                            "example": "Batata"
                                        },
                                        "price": {
                                            "type": "number",
                                            "example": 10
                                        },
                                        "createdAt": {
                                            "type": "string",
                                            "format": "date-time"
                                        },
                                        "productId": {
                                            "type": "string",
                                            "format": "uuid"
                                        }
                                    },
                                    "required": ["name", "price", "createdAt", "productId"]
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Failed validation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Bad Request"
                                        },
                                        "statusCode": {
                                            "type": "integer",
                                            "example": 400
                                        },
                                        "details": {
                                            "type": "array",
                                            "items": {
                                                "type": "object"
                                            },
                                            "example": []
                                        }
                                    },
                                    "required": ["message", "statusCode"]
                                }
                            }
                        }
                    }
                }
            }
        },
        "/products ": {
            "get": {
                "tags": ["Product"],
                "summary": "Product search",
                "operationId": "productFind",
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "type": "object",
                                        "properties": {
                                            "name": {
                                                "type": "string",
                                                "example": "Batata"
                                            },
                                            "price": {
                                                "type": "number",
                                                "example": 10
                                            },
                                            "createdAt": {
                                                "type": "string",
                                                "format": "date-time"
                                            },
                                            "productId": {
                                                "type": "string",
                                                "format": "uuid"
                                            }
                                        },
                                        "required": ["name", "price", "createdAt", "productId"]
                                    }
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Failed validation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Bad Request"
                                        },
                                        "statusCode": {
                                            "type": "integer",
                                            "example": 400
                                        },
                                        "details": {
                                            "type": "array",
                                            "items": {
                                                "type": "object"
                                            },
                                            "example": []
                                        }
                                    },
                                    "required": ["message", "statusCode"]
                                }
                            }
                        }
                    }
                }
            }
        },
        "/products/{id}": {
            "get": {
                "tags": ["Product"],
                "summary": "Product search by ID",
                "operationId": "productFindOne",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "schema": {
                            "type": "string",
                            "format": "uuid"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "name": {
                                            "type": "string",
                                            "example": "Batata"
                                        },
                                        "price": {
                                            "type": "number",
                                            "example": 10
                                        },
                                        "createdAt": {
                                            "type": "string",
                                            "format": "date-time"
                                        },
                                        "productId": {
                                            "type": "string",
                                            "format": "uuid"
                                        }
                                    },
                                    "required": ["name", "price", "createdAt", "productId"]
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "Successful operation"
                    },
                    "400": {
                        "description": "Failed validation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Bad Request"
                                        },
                                        "statusCode": {
                                            "type": "integer",
                                            "example": 400
                                        },
                                        "details": {
                                            "type": "array",
                                            "items": {
                                                "type": "object"
                                            },
                                            "example": []
                                        }
                                    },
                                    "required": ["message", "statusCode"]
                                }
                            }
                        }
                    }
                }
            }
        },
        "/purchases": {
            "post": {
                "tags": ["Purchase"],
                "summary": "Purchase registration",
                "operationId": "purchaseCreate",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "productId": {
                                        "type": "string",
                                        "format": "uuid"
                                    },
                                    "amount": {
                                        "type": "integer",
                                        "example": 1
                                    }
                                },
                                "required": ["productId", "amount"]
                            }
                        }
                    },
                    "required": true
                },
                "responses": {
                    "201": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "productId": {
                                            "type": "string",
                                            "format": "uuid"
                                        },
                                        "amount": {
                                            "type": "integer",
                                            "example": 1
                                        },
                                        "createdAt": {
                                            "type": "string",
                                            "format": "date-time"
                                        },
                                        "purchaseId": {
                                            "type": "string",
                                            "format": "uuid"
                                        }
                                    },
                                    "required": ["productId", "amount", "createdAt", "purchaseId"]
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Failed validation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Bad Request"
                                        },
                                        "statusCode": {
                                            "type": "integer",
                                            "example": 400
                                        },
                                        "details": {
                                            "type": "array",
                                            "items": {
                                                "type": "object"
                                            },
                                            "example": []
                                        }
                                    },
                                    "required": ["message", "statusCode"]
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Failed operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Not Found"
                                        },
                                        "statusCode": {
                                            "type": "integer",
                                            "example": 404
                                        },
                                        "details": {
                                            "type": "array",
                                            "items": {
                                                "type": "object"
                                            },
                                            "example": []
                                        }
                                    },
                                    "required": ["message", "statusCode"]
                                }
                            }
                        }
                    }
                }
            }
        },
        "/purchases/batch": {
            "post": {
                "tags": ["Purchase"],
                "summary": "Purchase batch registration",
                "operationId": "purchaseCreateBatch",
                "responses": {
                    "201": {
                        "description": "Successful operation"
                    },
                    "400": {
                        "description": "Failed validation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Bad Request"
                                        },
                                        "statusCode": {
                                            "type": "integer",
                                            "example": 400
                                        },
                                        "details": {
                                            "type": "array",
                                            "items": {
                                                "type": "object"
                                            },
                                            "example": []
                                        }
                                    },
                                    "required": ["message", "statusCode"]
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Failed operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Not Found"
                                        },
                                        "statusCode": {
                                            "type": "integer",
                                            "example": 404
                                        },
                                        "details": {
                                            "type": "array",
                                            "items": {
                                                "type": "object"
                                            },
                                            "example": []
                                        }
                                    },
                                    "required": ["message", "statusCode"]
                                }
                            }
                        }
                    }
                }
            }
        },
        "/purchases ": {
            "get": {
                "tags": ["Purchase"],
                "summary": "Purchase search",
                "operationId": "purchaseFind",
                "responses": {
                    "200": {
                        "description": "Successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "type": "object",
                                        "properties": {
                                            "productId": {
                                                "type": "string",
                                                "format": "uuid"
                                            },
                                            "amount": {
                                                "type": "integer",
                                                "example": 1
                                            },
                                            "createdAt": {
                                                "type": "string",
                                                "format": "date-time"
                                            },
                                            "purchaseId": {
                                                "type": "string",
                                                "format": "uuid"
                                            }
                                        },
                                        "required": ["productId", "amount", "createdAt", "purchaseId"]
                                    }
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Failed validation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "message": {
                                            "type": "string",
                                            "example": "Bad Request"
                                        },
                                        "statusCode": {
                                            "type": "integer",
                                            "example": 400
                                        },
                                        "details": {
                                            "type": "array",
                                            "items": {
                                                "type": "object"
                                            },
                                            "example": []
                                        }
                                    },
                                    "required": ["message", "statusCode"]
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
