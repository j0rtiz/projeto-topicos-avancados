const { Router } = require('express');
const Status = require('http-status');

/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/app/operations/purchase/purchaseCreateBatchOperation')} ctx.purchaseCreateBatchOperation
 * @param {import('src/interfaces/http/schemas/purchase/purchaseCreateSchema')} ctx.purchaseCreateSchema
 * @param {import('src/app/operations/purchase/purchaseCreateOperation')} ctx.purchaseCreateOperation
 * @param {import('src/interfaces/http/middlewares/validatorMiddleware')} ctx.validatorMiddleware
 * @param {import('src/app/operations/purchase/purchaseFindOperation')} ctx.purchaseFindOperation
 */
module.exports = ({
    purchaseCreateBatchOperation,
    purchaseCreateOperation,
    purchaseFindOperation,
    purchaseCreateSchema,
    validatorMiddleware
}) => ({
    purchaseCreate: async (req, res, next) => {
        try {
            const data = { ...req.body };
            const response = await purchaseCreateOperation.execute(data);

            return res.status(Status.CREATED).json(response);
        } catch (err) {
            next(err);
        }
    },
    purchaseCreateBatch: (req, res, next) => {
        try {
            purchaseCreateBatchOperation.execute(res);

            return res.status(Status.CREATED).end();
        } catch (err) {
            next(err);
        }
    },
    purchaseFind: (req, res, next) => {
        try {
            const response = purchaseFindOperation.execute();

            return res.status(Status.OK).json(response);
        } catch (err) {
            next(err);
        }
    },
    get router() {
        return Router()
            .post('/', validatorMiddleware(purchaseCreateSchema), this.purchaseCreate)
            .post('/batch', this.purchaseCreateBatch)
            .get('/', this.purchaseFind);
    }
});
