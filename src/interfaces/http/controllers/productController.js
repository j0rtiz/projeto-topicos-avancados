const { Router } = require('express');
const Status = require('http-status');

/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/interfaces/http/schemas/product/productFindOneSchema')} ctx.productFindOneSchema
 * @param {import('src/interfaces/http/schemas/product/productCreateSchema')} ctx.productCreateSchema
 * @param {import('src/app/operations/product/productFindOneOperation')} ctx.productFindOneOperation
 * @param {import('src/app/operations/product/productCreateOperation')} ctx.productCreateOperation
 * @param {import('src/interfaces/http/middlewares/validatorMiddleware')} ctx.validatorMiddleware
 * @param {import('src/app/operations/product/productFindOperation')} ctx.productFindOperation
 */
module.exports = ({
    productFindOneOperation,
    productCreateOperation,
    productFindOperation,
    productFindOneSchema,
    productCreateSchema,
    validatorMiddleware
}) => ({
    productCreate: (req, res, next) => {
        try {
            const data = { ...req.body };
            const response = productCreateOperation.execute(data);

            return res.status(Status.CREATED).json(response);
        } catch (err) {
            next(err);
        }
    },
    productFind: (req, res, next) => {
        try {
            const response = productFindOperation.execute();

            return res.status(Status.OK).json(response);
        } catch (err) {
            next(err);
        }
    },
    productFindOne: (req, res, next) => {
        try {
            const data = { ...req.params };
            const response = productFindOneOperation.execute(data);

            if (!response) {
                return res.status(Status.NO_CONTENT).end();
            }

            return res.status(Status.OK).json(response);
        } catch (err) {
            next(err);
        }
    },
    get router() {
        return Router()
            .post('/', validatorMiddleware(productCreateSchema), this.productCreate)
            .get('/', this.productFind)
            .get('/:id', validatorMiddleware(productFindOneSchema), this.productFindOne);
    }
});
