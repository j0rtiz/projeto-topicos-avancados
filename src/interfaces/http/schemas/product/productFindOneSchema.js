const joi = require('joi');

module.exports = () =>
    joi
        .object({
            params: joi
                .object({
                    id: joi.string().required()
                })
                .required()
        })
        .required();
