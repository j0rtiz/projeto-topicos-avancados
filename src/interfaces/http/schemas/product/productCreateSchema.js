const joi = require('joi');

module.exports = () =>
    joi
        .object({
            body: joi
                .object({
                    name: joi.string().required(),
                    price: joi.number().positive().precision(2).required()
                })
                .required()
        })
        .required();
