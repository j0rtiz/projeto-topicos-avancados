const joi = require('joi');

module.exports = () =>
    joi
        .object({
            body: joi
                .object({
                    productId: joi.string().required(),
                    amount: joi.number().positive().integer().required()
                })
                .required()
        })
        .required();
