/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/infra/integration/rest/httpClient')} ctx.httpClient
 * @param {import('src/infra/logging/logger')} ctx.logger
 * @param {import('config')} ctx.config
 */
module.exports = ({ httpClient, logger, config }) => {
    const HttpClient = httpClient(config.integration.rest.stock);

    return {
        stockFind: async (payload) => {
            try {
                const httpConfig = {
                    method: 'get',
                    url: '/api/stocks',
                    params: payload
                };
                const { data } = await HttpClient(httpConfig);

                return { data };
            } catch (error) {
                logger.error('Http request error when calling Stock Client to find product');

                return { error };
            }
        },
        stockUpdate: async (payload) => {
            try {
                const httpConfig = {
                    method: 'post',
                    url: '/api/stocks',
                    data: payload
                };
                const { data } = await HttpClient(httpConfig);

                return { data };
            } catch (error) {
                logger.error('Http request error when calling Stock Client to update product');

                return { error };
            }
        }
    };
};
