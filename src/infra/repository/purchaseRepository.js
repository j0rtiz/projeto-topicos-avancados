const uuidv4 = require('uuid').v4;

/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/infra/database/fakeDatabase')} ctx.fakeDatabase
 * @param {import('src/infra/error/exception')} ctx.exception
 */
module.exports = ({ fakeDatabase, exception }) => ({
    create(data) {
        try {
            Object.assign(data, { createdAt: new Date(), purchaseId: uuidv4() });
            fakeDatabase.purchases.push(data);

            return data;
        } catch (error) {
            throw exception.database(error);
        }
    },
    find() {
        try {
            return fakeDatabase.purchases;
        } catch (error) {
            throw exception.database(error);
        }
    }
});
