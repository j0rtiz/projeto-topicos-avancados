/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/app/services/product/productCreateService')} ctx.productCreateService
 */
module.exports = ({ productCreateService }) => ({
    execute: (data) => productCreateService.execute(data)
});
