/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/app/services/product/productFindOneService')} ctx.productFindOneService
 */
module.exports = ({ productFindOneService }) => ({
    execute: (data) => productFindOneService.execute(data)
});
