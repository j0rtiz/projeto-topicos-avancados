/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/app/services/product/productFindService')} ctx.productFindService
 */
module.exports = ({ productFindService }) => ({
    execute: () => productFindService.execute()
});
