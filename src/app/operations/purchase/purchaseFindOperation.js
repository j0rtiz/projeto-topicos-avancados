/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/app/services/purchase/purchaseFindService')} ctx.purchaseFindService
 */
module.exports = ({ purchaseFindService }) => ({
    execute: () => purchaseFindService.execute()
});
