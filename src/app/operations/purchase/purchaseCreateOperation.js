/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/app/services/purchase/purchaseCreateService')} ctx.purchaseCreateService
 */
module.exports = ({ purchaseCreateService }) => ({
    execute: async (data) => purchaseCreateService.execute(data)
});
