/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/app/services/purchase/purchaseCreateBatchService')} ctx.purchaseCreateBatchService
 */
module.exports = ({ purchaseCreateBatchService }) => ({
    execute: (res) => purchaseCreateBatchService.execute(res)
});
