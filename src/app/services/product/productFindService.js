/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/infra/repository/productRepository')} ctx.productRepository
 * @param {import('src/infra/logging/logger')} ctx.logger
 */
module.exports = ({ productRepository, logger }) => ({
    execute: () => {
        const result = productRepository.find();

        logger.info({ received: result });

        return result;
    }
});
