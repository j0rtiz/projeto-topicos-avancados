/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/infra/repository/productRepository')} ctx.productRepository
 * @param {import('src/infra/logging/logger')} ctx.logger
 */
module.exports = ({ productRepository, logger }) => ({
    execute: (data) => {
        const result = productRepository.findOne(data);

        logger.info({ received: result });

        return result;
    }
});
