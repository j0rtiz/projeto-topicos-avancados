const Status = require('http-status');

/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/infra/database/fakeDatabase')} ctx.fakeDatabase
 * @param {import('src/infra/logging/logger')} ctx.logger
 */
module.exports = ({ fakeDatabase, logger }) => ({
    execute: (res) => {
        const batch = [];

        for (const data of fakeDatabase.purchases) {
            const product = fakeDatabase.products.find(({ productId }) => productId === data.productId);

            Object.assign(product, data);

            if (product.price > 10) {
                batch.push(product);
            }
        }

        logger.info({ batch });

        res.writeHead(Status.CREATED, {
            'Content-Type': 'application/json',
            'Content-Disposition': 'attachment;filename=PRODUTOS_ACIMA_DE_10_REAIS.json'
        });
        res.end(Buffer.from(JSON.stringify(batch, null, 4)));
    }
});
