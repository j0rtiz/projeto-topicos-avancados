/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/infra/repository/purchaseRepository')} ctx.purchaseRepository
 * @param {import('src/infra/logging/logger')} ctx.logger
 */
module.exports = ({ purchaseRepository, logger }) => ({
    execute: () => {
        const result = purchaseRepository.find();

        logger.info({ received: result });

        return result;
    }
});
