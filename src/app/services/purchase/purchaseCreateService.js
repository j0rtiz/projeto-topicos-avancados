/**
 * @param {Object} ctx - Dependency Injection
 * @param {import('src/infra/repository/purchaseRepository')} ctx.purchaseRepository
 * @param {import('src/infra/integration/rest/stockClient')} ctx.stockClient
 * @param {import('src/infra/database/fakeDatabase')} ctx.fakeDatabase
 * @param {import('src/infra/error/exception')} ctx.exception
 * @param {import('src/infra/logging/logger')} ctx.logger
 */
module.exports = ({ purchaseRepository, stockClient, fakeDatabase, exception, logger }) => ({
    execute: async (payload) => {
        const hasProductInCatalog = fakeDatabase.products.some((product) => product.productId === payload.productId);

        if (!hasProductInCatalog) {
            throw exception.notFound('Product not found in the catalog');
        }

        const { error, data } = await stockClient.stockFind(payload)

        if (error) {
            throw error;
        }

        if (!data.hasAvailability) {
            throw exception.operation('Product not found in stock or does not have the requested quantity');
        }

        data.product.amount -= payload.amount
        await stockClient.stockUpdate(data.product)

        const result = purchaseRepository.create(payload);

        logger.info({ created: result });

        return result;
    }
});
